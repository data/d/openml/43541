# OpenML dataset: Americas-Top-College-Rankings-2019-(Forbes)

https://www.openml.org/d/43541

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Starting in 2008, every year Forbes Magazine publishes a list of America's best colleges.  Schools are ranked based on alumni salary (20), student satisfaction (20), debt (20), American leaders (15), on time graduation rate (12.5), and academic success (12.5). To learn more about the methodology, go to https://www.forbes.com/top-colleges/1208425d1987.
Content
The data set contains the rankings of 650 Unites States colleges along with various other statistics pertaining to each school.
Acknowledgements
Data collected from https://www.forbes.com/top-colleges/1208425d1987. Source was edited by Justin Conklin, Carter Coudriet, and Caroline Howard, and reported by Maria Clara Cobo, Julie Coleman, Madison Fernandez, Grace Kay, and Derek Saul.
Inspiration
Choosing where to go to college is one of the most prevalent issues facing teenagers across the country. With this data, I hope interesting findings will emerge pertaining to which schools are worth attending most.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43541) of an [OpenML dataset](https://www.openml.org/d/43541). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43541/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43541/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43541/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

